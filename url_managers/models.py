from django.db import models
from users.models import CustomeUser
from shortuuidfield import ShortUUIDField

class ShortUrl(models.Model):
  uuid = ShortUUIDField()
  c_user = models.ForeignKey(CustomeUser, on_delete = models.CASCADE, related_name = 's_urls')
  path = models.TextField(blank = True)
  long_url = models.TextField()

  def update_today_report(self, redirect_history):
    report = self.report_today
    ip_report = report.ip_reports.get_or_create(ip = redirect_history.ip)[0]
    report.update_browsers(redirect_history.browser_name)
    ip_report.update_browsers(redirect_history.browser_name)
    if redirect_history.is_mobile:
      ip_report.mobile_count += 1
      report.mobile_count += 1
    else:
      ip_report.desktop_count += 1
      report.desktop_count += 1
    ip_report.save()
    report.save()

  def update_all_reports(self):
    redirect_histories = self.redirect_histories
    self.report_today.update_report(redirect_histories, 0)
    self.report_yesterday.update_report(redirect_histories, 1)
    self.report_week.update_report(redirect_histories, 7)
    self.report_month.update_report(redirect_histories, 30)
    

  def get_report_by_time(self, time):
    if time == 'today':
      return self.report_today
    elif time == 'yesterday':
      return self.report_yesterday
    elif time == 'week':
      return self.report_week
    elif time == 'month':
      return self.report_month
    else:
      raise Exception("Invalid time")

class RedirectHistory(models.Model):
  short_url = models.ForeignKey(ShortUrl, on_delete = models.CASCADE, related_name = 'redirect_histories')
  ip = models.CharField(max_length = 20)
  browser_name = models.CharField(max_length = 100)
  is_mobile = models.BooleanField()
  created_at = models.DateTimeField(auto_now_add = True)
