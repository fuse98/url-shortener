from django.urls import path
from . import views

urlpatterns = [
    path('shorturl', views.UrlManagerView.as_view()),
    path('r/<slug:path>', views.UrlRedirectView.as_view()),
]
