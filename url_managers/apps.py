from django.apps import AppConfig


class UrlManagersConfig(AppConfig):
    name = 'url_managers'
