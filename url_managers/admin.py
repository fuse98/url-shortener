from django.contrib import admin

from .models import RedirectHistory, ShortUrl

admin.site.register(RedirectHistory)
admin.site.register(ShortUrl)
