from rest_framework import serializers
from .models import ShortUrl, RedirectHistory
from reports.models import ReportToday, ReportYesterday, ReportWeek, ReportMonth

class ShortUrlSerializer(serializers.ModelSerializer):

  class Meta:
    model = ShortUrl
    fields = ['path', 'long_url']  

  def create(self, validated_data):
    suggestion = validated_data.get('path', '')
    suggestion = suggestion.replace(' ', '')
    short_url = ShortUrl.objects.create(long_url = validated_data.pop('long_url'), c_user = self.context['c_user'])
    if not ShortUrl.objects.filter(path = suggestion).exists():
      short_url.path = suggestion
    else:
      short_url.path = str(short_url.uuid) + '_' + suggestion
    short_url.save()
    ReportToday.objects.create(short_url = short_url)
    ReportYesterday.objects.create(short_url = short_url)
    ReportWeek.objects.create(short_url = short_url)
    ReportMonth.objects.create(short_url = short_url)    
    return short_url

class ShortUrlListSerializer(serializers.ModelSerializer):
  short_url =  serializers.SerializerMethodField('create_short_url_string')
  class Meta:
    model = ShortUrl
    fields = ['short_url']

  def create_short_url_string(self, obj):
    BASE = "http://localhost:8000/r/"
    return BASE + obj.path


class RedirectHistorySerializer(serializers.ModelSerializer):
  class Meta:
    model = RedirectHistory
    fields = ['ip', 'browser_name', 'is_mobile']