from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status

from django.shortcuts import redirect
from .serializers import ShortUrlSerializer, ShortUrlListSerializer, RedirectHistorySerializer
from .models import ShortUrl, RedirectHistory
from user_agents import parse

from reports.tasks import save_redirect_history_and_update_today_report

def create_short_url_string(path):
  BASE = "http://localhost:8000/r/"
  return BASE + path


class UrlManagerView(APIView):
  
  def get_permissions(self):
    return [IsAuthenticated()]

  def post(self, request):
    serializer = ShortUrlSerializer(data = request.data, context = {'c_user': request.user.c_user})
    if serializer.is_valid():
      serializer.save()
      path = serializer.data['path']
      return Response(create_short_url_string(path), status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

  def get(self, request):
    short_urls = request.user.c_user.s_urls
    serializer = ShortUrlListSerializer(short_urls, many = True)
    return Response(serializer.data, status = status.HTTP_200_OK)

class UrlRedirectView(APIView):  

  def get(self, request, path):
    try:
      short_url = ShortUrl.objects.get(path = path)
      if not short_url:  
        return Response("Record not found", status = status.HTTP_404_NOT_FOUND)  
      u_data = request.META['HTTP_USER_AGENT']
      parsed_user_data = parse(u_data)
      data = {
        'browser_name': parsed_user_data.browser.family,
        'is_mobile': parsed_user_data.is_mobile,
        'ip': request.META.get('REMOTE_ADDR'),
        'short_url': short_url.id,
      }
      save_redirect_history_and_update_today_report.delay(data)
      return redirect(short_url.long_url)
    except:
      return Response("Bad request", status = status.HTTP_400_BAD_REQUEST)
    
    
  
