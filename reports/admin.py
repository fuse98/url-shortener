from django.contrib import admin

# Register your models here.
from .models import ReportToday, ReportYesterday, ReportWeek, ReportMonth, Browser

admin.site.register(ReportToday)
admin.site.register(ReportYesterday)
admin.site.register(ReportWeek)
admin.site.register(ReportMonth)
admin.site.register(Browser)