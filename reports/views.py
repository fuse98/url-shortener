from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from .serializers import ReportSerializer

class ReportView(APIView):

  def get_permissions(self):
    return [IsAuthenticated()]

  def get(self, request):
    try:
      c_user = request.user.c_user
      short_url = c_user.s_urls.filter(path = request.data['path']).first()
      report = short_url.get_report_by_time(request.data['time_type'])
      if not short_url:
        return Response("Record not found", status = status.HTTP_404_NOT_FOUND)
      serializer = ReportSerializer(report)
      return Response(serializer.data, status = status.HTTP_200_OK)

    except:
      return Response("Invalid request", status = status.HTTP_400_BAD_REQUEST)