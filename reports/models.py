from django.db import models
from enum import Enum

from url_managers.models import ShortUrl
from datetime import timedelta, datetime


class Report(models.Model):
  mobile_count = models.IntegerField(default = 0)
  desktop_count = models.IntegerField(default = 0)

  def reset_report(self):
    self.ip_reports.all().delete()
    self.browsers.all().delete()
    self.mobile_count = 0
    self.desktop_count = 0
    self.save()

  def consider_redirect_history(self, redirect_history):
    ip_report = self.ip_reports.get_or_create(ip = redirect_history.ip)[0]
    self.update_browsers(redirect_history.browser_name)
    ip_report.update_browsers(redirect_history.browser_name)
    if redirect_history.is_mobile:
      ip_report.mobile_count += 1
      self.mobile_count += 1
    else:
      ip_report.desktop_count += 1
      self.desktop_count += 1
    ip_report.save()
    self.save()
    
  def update_report(self, redirect_histories, duration_time):
    self.reset_report()
    time_threshold = datetime.now() - timedelta(days = duration_time)
    valid_rh = redirect_histories.filter(created_at__gt = time_threshold)
    for rh in valid_rh.all():
      self.consider_redirect_history(rh)
    self.save()
    
  def update_browsers(self, browser_name):
    browser = self.browsers.get_or_create(name = browser_name)[0]
    browser.count += 1
    browser.save()

class IpReport(models.Model):
  main_report = models.ForeignKey(Report, on_delete = models.CASCADE, related_name = 'ip_reports')
  ip = models.CharField(max_length = 20)
  mobile_count = models.IntegerField(default = 0)
  desktop_count = models.IntegerField(default = 0)

  def update_browsers(self, browser_name):
    browser = self.browsers.get_or_create(name = browser_name)[0]
    browser.count += 1
    browser.save()

class ReportToday(Report):
  short_url = models.OneToOneField(ShortUrl, on_delete = models.CASCADE, related_name = 'report_today')

class ReportYesterday(Report):
  short_url = models.OneToOneField(ShortUrl, on_delete = models.CASCADE, related_name = 'report_yesterday')
 

class ReportWeek(Report):
  short_url = models.OneToOneField(ShortUrl, on_delete = models.CASCADE, related_name = 'report_week')

class ReportMonth(Report):
  short_url = models.OneToOneField(ShortUrl, on_delete = models.CASCADE, related_name = 'report_month')
  
class Browser(models.Model):
  name = models.CharField(max_length = 100)
  count = models.IntegerField(default = 0)
  ip_report = models.ForeignKey(IpReport, on_delete = models.CASCADE, related_name = 'browsers', blank=True, null=True)
  main_report = models.ForeignKey(Report, on_delete = models.CASCADE, related_name = 'browsers', blank=True, null=True)
