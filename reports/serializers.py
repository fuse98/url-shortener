from rest_framework import serializers
from rest_framework.authtoken.models import Token

from rest_framework.validators import ValidationError
from .models import ReportToday
# from django.db.models import Q

from reports.models import IpReport, Browser


class BrowserSerializer(serializers.ModelSerializer):
  class Meta:
    model = Browser
    fields = ['name', 'count']

class IpReportSerializer(serializers.ModelSerializer):
  browsers = BrowserSerializer(many = True)
  class Meta:
    model = IpReport
    fields = ['ip', 'mobile_count', 'desktop_count', 'browsers']
  

class ReportSerializer(serializers.Serializer):
  
  tottal_count = serializers.SerializerMethodField('calculate_tottal_count') 
  mobile_count = serializers.SerializerMethodField('calculate_mobile_count')
  desktop_count = serializers.SerializerMethodField('calculate_desktop_count')
  browsers = serializers.SerializerMethodField('serialize_browsers_data')
  ip_reports = serializers.SerializerMethodField('serialize_ip_reports_data')
  
  def serialize_browsers_data(self, obj):
    return BrowserSerializer(obj.browsers, many = True).data

  def serialize_ip_reports_data(self, obj):
    return IpReportSerializer(obj.ip_reports, many = True).data
  
  def calculate_tottal_count(self, obj):
    return obj.mobile_count + obj.desktop_count
  def calculate_mobile_count(self, obj):
    return obj.mobile_count
  def calculate_desktop_count(self, obj):
    return obj.desktop_count

  def validate(self, data):
    return data
