from celery.decorators import task, periodic_task
from celery.utils.log import get_task_logger
from celery.task.schedules import crontab


from url_managers.models import RedirectHistory, ShortUrl
from user_agents import parse
from datetime import timedelta
    
logger = get_task_logger(__name__)

@task(name = "save_redirect_history_and_update_today_report_task")
def save_redirect_history_and_update_today_report(data):
  short_url = ShortUrl.objects.get(id = data['short_url'])
  redirect_history = RedirectHistory.objects.create(ip = data['ip'],
    browser_name = data['browser_name'], is_mobile = data['is_mobile'], short_url_id = data['short_url'])
  short_url.report_today.consider_redirect_history(redirect_history)


@periodic_task(name = "daily_update_reports_task", run_every = crontab(hour = 0, minute = 0))
def daily_update_reports_task():
  for short_url in ShortUrl.objects.all():
    short_url.update_all_reports()
    
  
  


