from rest_framework import serializers
from rest_framework.authtoken.models import Token

from rest_framework.validators import ValidationError
from .models import CustomeUser, User
from django.db.models import Q

class UserCreateSerializer(serializers.ModelSerializer):
  
  class Meta:
    model = User
    fields = ['username', 'email', 'password']

class UserSigninSerializer(serializers.ModelSerializer):
  token = serializers.CharField(allow_blank = True, read_only = True)
  username = serializers.CharField(required = False, allow_blank = True)
  email = serializers.EmailField(required = False, allow_blank = True)
  class Meta:
    model = User
    fields = ['username', 'email', 'password', 'token']
  
  def validate(self, data):
    email = data.get("email")
    username = data.get("username")
    password = data.get("password")
    if not email and  not username:
      raise ValidationError("a username or email is required to sign in!")

    user = User.objects.filter(Q(email = email) | Q(username = username)).distinct()
    if user.exists():
      user = user.first()
    else:
      raise ValidationError("invalid username/email.")
    if user:
      print(user.username, ' ', user.email)
      if not check_password(user, password):
        raise ValidationError("incorrect password or usernaem/email.")

    data["token"] = Token.objects.create(user = user)
    return data


def check_password(user, password):
  return user.password == password

class RegisterUserSerializer(serializers.ModelSerializer):
  user = UserCreateSerializer()
  class Meta:
    model = CustomeUser
    fields = ['user',]

  def create(self, validated_data):
    user_data = validated_data.pop('user')
    user = UserCreateSerializer.create(UserCreateSerializer(), validated_data=user_data)
    cuser = CustomeUser.objects.create(user = user)
    return cuser


class SigninUserSerializer(serializers.ModelSerializer):
  user = UserSigninSerializer()
  class Meta:
    model = CustomeUser
    fields = ['user',]

  def validate(self, data):
    user_data = data.pop('user')
    user_serializer = UserSigninSerializer(data = user_data)
    if user_serializer.is_valid():
      new_data = user_serializer.data
      return new_data
    raise ValidationError('authentication failed!!')
