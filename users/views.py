from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from .serializers import RegisterUserSerializer, UserSigninSerializer

# Create your views here.


class RegisterView(APIView):

  def post(self, request):

    serializer = RegisterUserSerializer(data = request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)




class SigninView(APIView):

  def post(self, request):
    serializer = UserSigninSerializer(data = request.data)
    if serializer.is_valid():
      new_data = serializer.data
      return Response(new_data, status = status.HTTP_200_OK)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
